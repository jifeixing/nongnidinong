package com.wstro.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 分类表
 */
@Data
@TableName("question")
public class QuestionEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @TableId(type = IdType.AUTO)
    private Integer Id;

    /**
     * 用户名
     */
    @TableField
    @NotEmpty(message = "试题题干")
    private String title;

    @TableField
    @NotNull(message = "试题分类ID")
    private Integer group;

    @TableField(exist = false)
    private String groupName;

    @TableField
    @NotNull(message = "试题类型")
    private Integer type;

    @TableField
    @NotNull(message = "cases")
    private String cases;

    @TableField
    @NotNull(message = "结果")
    private String result;

    @TableField(exist = false)
    private String typeName;

    @TableField(exist = false)
    private String casesxuanzeA;

    @TableField(exist = false)
    private String casesxuanzeB;

    @TableField(exist = false)
    private String casesxuanzeC;

    @TableField(exist = false)
    private String casesxuanzeD;

    @TableField(exist = false)
    private String casesdanxuan;

    @TableField(exist = false)
    private String casesduoxuan;



    @TableField
    @NotNull(message = "判断结果")
    private Integer casespanduan;

    @TableField
    private Long createTime;

    @TableField
    private Integer status;
}
