package com.wstro.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 分类表
 */
@Data
@TableName("question_group")
public class QuestionGroupEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId(type = IdType.AUTO)
    private Integer Id;

    /**
     * 用户名
     */
    @TableField
    @NotEmpty(message = "试题分类名称")
    private String name;


    @TableField
    @NotNull(message = "状态")
    private Integer status;


}
