package com.wstro.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * created by qianliu.song
 **/
@Data
@TableName("question")
public class OptionEntity  implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Integer Id;

    @TableField
    @NotEmpty(message = "试题ID")
    private String questionId;
    @TableField
    @NotEmpty(message = "头")
    private String header;
    @TableField
    @NotEmpty(message = "体")
    private String body;
}
