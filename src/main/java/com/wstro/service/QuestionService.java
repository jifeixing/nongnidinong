package com.wstro.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.wstro.entity.QuestionEntity;
import com.wstro.entity.QuestionGroupEntity;

/**
 * 系统用户
 *
 * @author Joey
 * @Email 2434387555@qq.com
 */
public interface QuestionService extends IService<QuestionEntity> {

    Page<QuestionEntity> queryListByPage(Integer offset, Integer limit, String userName, String sort, Boolean flag);

    void save(QuestionEntity questionGroupEntity);
}
