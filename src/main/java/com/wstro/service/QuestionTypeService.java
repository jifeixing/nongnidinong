package com.wstro.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.wstro.entity.QuestionEntity;
import com.wstro.entity.QuestionTypeEntity;

/**
 * 系统用户
 *
 * @author Joey
 * @Email 2434387555@qq.com
 */
public interface QuestionTypeService extends IService<QuestionTypeEntity> {

}
