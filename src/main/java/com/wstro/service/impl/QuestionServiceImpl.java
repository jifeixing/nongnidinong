package com.wstro.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.wstro.dao.QuestionDao;
import com.wstro.dao.QuestionGroupDao;
import com.wstro.entity.QuestionEntity;


import com.wstro.service.QuestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * created by qianliu.song
 **/
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionDao, QuestionEntity> implements QuestionService {
    @Resource
    private QuestionDao questionDao;

    @Override
    public Page<QuestionEntity> queryListByPage(Integer offset, Integer limit, String title, String sort, Boolean order) {
        Wrapper<QuestionEntity> wrapper = new EntityWrapper<QuestionEntity>();
        if (StringUtils.isNoneBlank(sort) && null != order) {
            wrapper.orderBy(sort, order);
        }
        if (StringUtils.isNoneBlank(title)) {
            wrapper.like("title", title);
        }
        Page<QuestionEntity> page = new Page<>(offset, limit);
        return this.selectPage(page, wrapper);
    }

    @Override
    @Transactional
    public void save(QuestionEntity questionEntity) {
        questionDao.insert(questionEntity);
    }
}
