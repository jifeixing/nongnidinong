package com.wstro.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.wstro.dao.QuestionGroupDao;
import com.wstro.dao.QuestionTypeDao;
import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.QuestionTypeEntity;
import com.wstro.service.QuestionGroupService;
import com.wstro.service.QuestionTypeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * created by qianliu.song
 **/
@Service
public class QuestionTypeServiceImpl extends ServiceImpl<QuestionTypeDao, QuestionTypeEntity> implements QuestionTypeService {
    @Resource
    private QuestionTypeDao questionTypeDao;

}
