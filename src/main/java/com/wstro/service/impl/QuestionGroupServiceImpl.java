package com.wstro.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.wstro.dao.QuestionGroupDao;
import com.wstro.dao.SysUserDao;
import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.SysUserEntity;
import com.wstro.service.QuestionGroupService;
import com.wstro.util.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * created by qianliu.song
 **/
@Service
public class QuestionGroupServiceImpl extends ServiceImpl<QuestionGroupDao, QuestionGroupEntity> implements QuestionGroupService {
    @Resource
    private QuestionGroupDao questionGroupDao;

    @Override
    public Page<QuestionGroupEntity> queryListByPage(Integer offset, Integer limit, String name, String sort, Boolean order) {
        Wrapper<QuestionGroupEntity> wrapper = new EntityWrapper<QuestionGroupEntity>();
        if (StringUtils.isNoneBlank(sort) && null != order) {
            wrapper.orderBy(sort, order);
        }
        if (StringUtils.isNoneBlank(name)) {
            wrapper.like("name", name);
        }
        Page<QuestionGroupEntity> page = new Page<>(offset, limit);
        return this.selectPage(page, wrapper);
    }

    @Override
    @Transactional
    public void save(QuestionGroupEntity questionGroupEntity) {
        questionGroupDao.insert(questionGroupEntity);
    }
}
