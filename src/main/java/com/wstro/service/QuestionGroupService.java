package com.wstro.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.SysUserEntity;

import java.util.List;

/**
 * 系统用户
 *
 * @author Joey
 * @Email 2434387555@qq.com
 */
public interface QuestionGroupService extends IService<QuestionGroupEntity> {

    Page<QuestionGroupEntity> queryListByPage(Integer offset, Integer limit, String userName, String sort, Boolean flag);

    void save(QuestionGroupEntity questionGroupEntity);
}
