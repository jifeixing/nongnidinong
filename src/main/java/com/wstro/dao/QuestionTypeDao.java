package com.wstro.dao;

import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.QuestionTypeEntity;
import com.wstro.util.BaseDao;


public interface QuestionTypeDao extends BaseDao<QuestionTypeEntity> {

}
