package com.wstro.dao;

import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.SysUserEntity;
import com.wstro.util.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface QuestionGroupDao extends BaseDao<QuestionGroupEntity> {

}
