package com.wstro.dao;

import com.wstro.entity.QuestionEntity;
import com.wstro.util.BaseDao;


public interface QuestionDao extends BaseDao<QuestionEntity> {

}
