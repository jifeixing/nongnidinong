package com.wstro.controller.admin;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.wstro.entity.QuestionGroupEntity;
import com.wstro.entity.SysRoleEntity;
import com.wstro.entity.SysUserEntity;
import com.wstro.service.QuestionGroupService;
import com.wstro.service.SysUserLoginLogService;
import com.wstro.service.SysUserRoleService;
import com.wstro.service.SysUserService;
import com.wstro.util.PageUtils;
import com.wstro.util.R;
import com.wstro.util.ShiroUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

/**
 * 题目类型
 */
@Controller
@RequestMapping("admin/sys/question/group")
public class QuestionGroupController extends AbstractController {

    @Resource
    private QuestionGroupService questionGroupService;


    /**
     * 自己的个人信息
     *
     * @return
     */
    @RequestMapping("/getQuestion")
    @RequiresPermissions("sys:user:list")
    @ResponseBody
    public R getQuestion(Integer offset, Integer limit, String sort, String order,
                  @RequestParam(name = "search", required = false) String search) {
        String name = null;
        Map<String, String> searchList = parseObject(search, "name");
        if (null != searchList) {
            name = searchList.get("name");
        }
        offset = (offset / limit) + 1;
        Boolean flag = null; // 排序逻辑
        if (StringUtils.isNoneBlank(order)) {
            if (order.equalsIgnoreCase("asc")) {
                flag = true;
            } else {
                flag = false;
            }
        }
        Page<QuestionGroupEntity> questionGroupEntityPage = questionGroupService.queryListByPage(offset, limit, name, sort, flag);
        PageUtils pageUtil = new PageUtils(questionGroupEntityPage.getRecords(), questionGroupEntityPage.getTotal(), questionGroupEntityPage.getSize(),
                questionGroupEntityPage.getCurrent());
        return R.ok().put("page", pageUtil);
    }

    @RequestMapping("/list")
    //@RequiresPermissions("sys:role:select")
    @ResponseBody
    public R list() {
        // 查询列表数据
        List<QuestionGroupEntity> list = questionGroupService.selectList(null);

        return R.ok().put("list", list);
    }

    @RequestMapping("/getQuestionById/{id}")
    @RequiresPermissions("sys:user:list")
    @ResponseBody
    public R getQuestionById(@PathVariable("id") Long id) {
        QuestionGroupEntity questionGroupEntity = questionGroupService.selectById(id);



        return R.ok().put("question", questionGroupEntity);
    }

    @RequestMapping("/save")
    @ResponseBody
    //@RequiresPermissions("sys:user:save")
    public R save(@Valid QuestionGroupEntity questionGroupEntity, BindingResult result)
            throws Exception {
        questionGroupEntity.setStatus(1);
        questionGroupService.save(questionGroupEntity);
        return R.ok();
    }
    @RequestMapping("/update")
    @ResponseBody
    //@RequiresPermissions("sys:user:save")
    public R update(@Valid QuestionGroupEntity questionGroupEntity, BindingResult result)
            throws Exception {
        questionGroupEntity.setStatus(1);
        questionGroupService.updateById(questionGroupEntity);
        return R.ok();
    }
    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions("sys:user:delete")
    public R delete(@RequestParam("ids") String ids) {
        JSONArray jsonArray = JSONArray.parseArray(ids);
        Long[] idArray = toArrays(jsonArray);
        if (idArray.length < 1) {
            return R.error("删除的试题分类为空");
        }

        for (int i = 0; i < idArray.length; i++) {
            questionGroupService.deleteById(idArray[i].intValue());
        }
        return R.ok();
    }

}
