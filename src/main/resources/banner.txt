                                   _         _ _
                                  (_)       | (_)
 _ __   ___  _ __   __ _     _ __  _      __| |_     _ __   ___  _ __   __ _
| '_ \ / _ \| '_ \ / _` |   | '_ \| |    / _` | |   | '_ \ / _ \| '_ \ / _` |
| | | | (_) | | | | (_| |   | | | | |   | (_| | |   | | | | (_) | | | | (_| |
|_| |_|\___/|_| |_|\__, |   |_| |_|_|    \__,_|_|   |_| |_|\___/|_| |_|\__, |
                    __/ |                                               __/ |
                   |___/                                               |___/
${AnsiColor.BLUE}
application.version:${application.version} 
application.formatted-version:${application.formatted-version}                         
spring-boot.version:${spring-boot.version}  
spring-boot.formatted-version:${spring-boot.formatted-version}